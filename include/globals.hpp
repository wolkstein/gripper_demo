/**
 * @file globals.hpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief Project Global Vars
 * @version 0.1
 * @date 2021-06-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef GLOBALS_H
#define GLOBALS_H

// if we use a globals.h file, we make our global includes here. (pid and TLD)
#include <Arduino.h>
#include <PID_v1.h>
#include "tea_linear_drive.hpp"


// Declaration of Global Variables

extern elapsedMillis __faderloop;
extern uint32_t __faderlooptime;
extern elapsedMillis __seqloop;
extern uint32_t __seqlooptime;

extern int __sequence[];
extern int __step;
extern bool __motordriverActive;

extern double __aggKp, __aggKi, __aggKd;
extern double __consKp, __consKi, __consKd;
extern double __positionP, __outputP, __targetP;

extern String serialstring;
extern String teststring;
extern int baaar;

// create Objects
extern PID myPID;
extern TEA_LINEAR_DRIVE TLD;


#endif
