/**
 * @file TEA_LINEAR_DRIVE.hpp
 * @author Michael WOlkstein (mw@3rd-element.com)
 * @brief Linear Slider Class
 * @version 0.1
 * @date 2021-06-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef TEA_LINEAR_DRIVE_H
#define TEA_LINEAR_DRIVE_H

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

/**
 * @brief Third Element Aviation Linear Slider Device Class
 * 
 */
class TEA_LINEAR_DRIVE  
{

  public:

	/**
	 * @brief Example Definition
	 * 
	 */
	#define SOMETHINGIMPORTANT 101

	/**
	 * @brief Construct a new tea linear drive object
	 * 
	 * @param pwmPin 
	 * @param directionPin 
	 * @param enablePin 
	 * @param potiPin 
	 */
	TEA_LINEAR_DRIVE(int pwmPin, int directionPin, int enablePin, int potiPin);
	
	/**
	 * @brief drive Motor with direction and speed 
	 * 
	 * @param direction 0 = ccw, 1 = cw 
	 * @param speed 0 - 255 
	 */
	void drive(int direction, int speed);
	
	/**
	 * @brief enable motordriver
	 * 
	 * @return true 
	 * @return false 
	 */
	bool enable();
	
	/**
	 * @brief disable motordriver
	 * 
	 * @return true 
	 * @return false 
	 */
	bool disable();
	
	/**
	 * @brief Get the Position of Linearslider
	 * 
	 * @return int 
	 */
	int getPosition();

	    
	/**
	 * @brief Init Lineardriver in arduino setup() function
	 * 
	 */
	void Initialize();

  private:

	
	int motorPWM_Pin;
	int motorDIR_Pin;
	int enable_Pin;
	int poti_Pin;

};
#endif