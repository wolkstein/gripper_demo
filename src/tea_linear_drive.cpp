/**
 * @file TEA_LINEAR_DRIVE.cpp
 * @author Michael WOlkstein (mw@3rd-element.com)
 * @brief Linear Slider Class
 * @version 0.1
 * @date 2021-06-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "tea_linear_drive.hpp"
 

TEA_LINEAR_DRIVE::TEA_LINEAR_DRIVE(int pwmPin, int directionPin, int enablePin, int potiPin)
{
  motorPWM_Pin = pwmPin;
	motorDIR_Pin = directionPin;
	enable_Pin = enablePin;
	poti_Pin = potiPin;	
}

void TEA_LINEAR_DRIVE::Initialize()
{
    pinMode(motorDIR_Pin, OUTPUT);
    pinMode(enable_Pin, OUTPUT );

}

void TEA_LINEAR_DRIVE::drive(int direction, int speed)
{
    digitalWrite(motorDIR_Pin, direction);
    analogWrite(motorPWM_Pin, speed);
}

bool TEA_LINEAR_DRIVE::enable()
{
  // ! Driver Pin must LOW to enable the Driver
  digitalWrite(enable_Pin, LOW);
  // Return Driver Status 
  return !digitalRead(enable_Pin); 
}

bool TEA_LINEAR_DRIVE::disable()
{
  digitalWrite(enable_Pin, HIGH);
  return !digitalRead(enable_Pin);
}

int TEA_LINEAR_DRIVE::getPosition()
{
    return analogRead(poti_Pin);
}
