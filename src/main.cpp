/**
 * @file main.cpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief TEA Example Code 
 * @version 0.1
 * @date 2021-06-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "globals.hpp"

/* function declaration of some extra functions in main.cpp*/
int foo(int bar);
void doFaderJob(uint32_t looptime);
void doSequencer(uint32_t seqtime);
void checkSerialInput();

/**
 * @brief Arduino Setup Function
 * 
 */
void setup() {
  // Debug output on usb serial
  Serial.begin(9600);
  
  // setup myPID Object with good settings
  myPID.SetMode(AUTOMATIC);
  myPID.SetSampleTime(1);
  myPID.SetTunings(__aggKp, __aggKi, __aggKd);
  myPID.SetOutputLimits(-255,255);
  
  TLD.Initialize();
  __motordriverActive = TLD.enable();

  // example value for PID target
  // to drive the slider it is only nessesary to change this value
  __targetP = 777;

}

/**
 * @brief Arduino Main Loop Function
 * 
 */
void loop() {
  
  // targetP is the only value that must change to drive the slider
  // for example we can control targetP with an statemachine from extanel input
  // serial or serial mavlink or i2c or whatever

  // check serial input
  checkSerialInput();

  // process Fader
  doFaderJob(__faderlooptime);

  // example slide position sequencer
  //doSequencer(__seqlooptime);


}// ~loop()



//----------------8<-------------------

// Implementation of Extrafunctions in main.cpp


/**
 * @brief example function
 * 
 * @param bar 
 * @return int 
 */
int foo(int bar)
{
    return bar + 1;
}

/**
 * @brief do the fader/motor pid job
 * 
 * @param looptime set the time for next loop default 1ms -> 1s/1000ms = 1000hz
 */
void doFaderJob(uint32_t looptime)
{
  // example process fader in mainloop by timer. Idele time must be under control 
  if(__faderloop >= looptime){
    
    __positionP = TLD.getPosition();
    
    myPID.Compute();
    double gap = abs(__targetP-__positionP);
    int dynspeed = int(__outputP);

    if(__outputP > 0) { // drive ccw
      TLD.drive(0, int(abs(dynspeed)));
    }
    else {          // drive cw
      TLD.drive(1, int(abs(dynspeed)));
    }
    if (gap < 5){  //we're close to setpoint, we disable the motordriver to hold the system cold
      if (__motordriverActive) __motordriverActive = TLD.disable();  
    }
    else {
      if (!__motordriverActive) __motordriverActive = TLD.enable();
    }

    __faderloop = 0;
  }
}

/**
 * @brief process 
 * 
 * @param seqtime 
 */
void doSequencer(uint32_t seqtime)
{
  if(__seqloop >= seqtime){
    __targetP = __sequence[__step];
    __step++;
    if(__step > 7) __step = 0;
    Serial.printf("Time: %d, Step: %d: Ziel: %d, DriverSTatus %d, %s, Extrafunction-Test: %d \n",
                    millis(),__step,int(__targetP),__motordriverActive, teststring.c_str(), baaar = foo(baaar));
    __seqloop = 0;
  }
}

void checkSerialInput()
{
  while (Serial.available()) {
    if (Serial.available() >0) {
      char c = Serial.read();  //gets one byte from serial buffer
      serialstring += c; //makes the string readString
    } 
  }

  // convert string to int and set __targetP with valid values
  if (serialstring.length() > 0) {
      String sworker = serialstring; 
      serialstring = "";
      int testinteger = sworker.toInt();
      Serial.println(testinteger);
      if(testinteger <= 10) testinteger = 10;
      if(testinteger >= 1000) testinteger = 1000;
       __targetP = testinteger;
  }
}
