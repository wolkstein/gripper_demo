/**
 * @file globals.cpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief Project Global Vars
 * @version 0.1
 * @date 2021-06-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "globals.hpp"  

// Definition of Global Variables
elapsedMillis __faderloop;
uint32_t __faderlooptime = 1;
elapsedMillis __seqloop;
uint32_t __seqlooptime = 10000;

int __sequence[9] = { 20, 1000, 500, 1000, 10, 550, 200, 800};
int __step = 0;
bool __motordriverActive = false;

double __aggKp = 20.06, __aggKi = 20.72, __aggKd = 0.021;
double __consKp = __aggKp/4, __consKi = __aggKi, __consKd=__aggKd/10;
double __positionP=0, __outputP=0, __targetP=0;

PID myPID(&__positionP, &__outputP, &__targetP, __aggKp,__aggKi ,__aggKd , DIRECT);
TEA_LINEAR_DRIVE TLD(22, 21, 20, A10);

String teststring = "ohjeoje_ist_das_schreag";
String serialstring = "";
int baaar = 0;
